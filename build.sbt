name := "dokka-aplos"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  val akkaVersion       = "2.4.14"
  val akkaHttpVersion   = "10.1.4"

  Seq(
    "com.typesafe.akka"               %% "akka-actor"                     % akkaVersion,
    "com.typesafe.akka"               %% "akka-stream"                    % akkaVersion,
    "com.typesafe.akka"               %% "akka-slf4j"                     % akkaVersion,
    "io.spray"                        %% "spray-json"                     % "1.3.5",
    "com.typesafe.akka"               %% "akka-stream-kafka"              % "0.21.1",
    "com.lightbend.akka"              %% "akka-stream-alpakka-cassandra"  % "1.0-M1",
    "com.typesafe.akka"               %% "akka-http"                      % akkaHttpVersion,
    "com.typesafe.akka"               %% "akka-http-spray-json"           % akkaHttpVersion,
    "org.json4s"                      %% "json4s-jackson"                 % "3.6.4",
    "org.elasticsearch.client"        % "transport"                       % "6.2.3",
    "com.jcraft"                      % "jsch"                            % "0.1.54",
    "redis.clients"                   % "jedis"                           % "2.4.2",
    "com.itextpdf"                    % "itext7-core" % "7.1.0" pomOnly(),
    "org.apache.logging.log4j"        % "log4j-api"                       % "2.6.2",
    "org.apache.logging.log4j"        % "log4j-to-slf4j"                  % "2.6.2",
    "ch.qos.logback"                  % "logback-classic"                 % "1.0.9",
    "org.scalatest"                   % "scalatest_2.11"                  % "2.2.1"               % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".RSA" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".keys" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".logs" => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
