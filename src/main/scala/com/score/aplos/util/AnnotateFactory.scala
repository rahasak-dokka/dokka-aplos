package com.score.aplos.util

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}

import com.itextpdf.forms.PdfAcroForm
import com.itextpdf.forms.fields.PdfFormField
import com.itextpdf.kernel.geom.Rectangle
import com.itextpdf.kernel.pdf.annot.PdfWidgetAnnotation
import com.itextpdf.kernel.pdf.{PdfDocument, PdfReader, PdfWriter}
import com.score.aplos.config.AnnotateConfig
import com.score.aplos.protocol.Annotation
import sun.misc.{BASE64Decoder, BASE64Encoder}

import scala.util.Try

object AnnotateFactory extends AnnotateConfig {

  def toAnnotate(params: List[String]): Option[Annotation] = {
    Try {
      Annotation(params.head, params(1).toFloat, params(2).toFloat, params(3).toInt)
    }.toOption
  }

  def annotate(annotations: List[Annotation], blob: String): String = {
    // reader
    val bais = new ByteArrayInputStream(new BASE64Decoder().decodeBuffer(blob))
    val reader: PdfReader = new PdfReader(bais)

    // writer
    val baos = new ByteArrayOutputStream()
    val writer = new PdfWriter(baos)

    // pdf document
    val pdfDoc = new PdfDocument(reader, writer)
    val form = PdfAcroForm.getAcroForm(pdfDoc, true)

    // add text field annotations
    annotations.foreach { annotation =>
      val x = (annotation.x / 100) * pdfDoc.getPage(1).getPageSizeWithRotation.getWidth
      val y = (annotation.y / 100) * pdfDoc.getPage(1).getPageSizeWithRotation.getHeight

      // add text field and annotation
      // set width and height as 100 points
      val textField = PdfFormField.createText(pdfDoc)
      val widgetAnnotation = new PdfWidgetAnnotation(new Rectangle(x, y, 100, 100))
      widgetAnnotation.makeIndirect(pdfDoc)
      pdfDoc.getFirstPage.addAnnotation(widgetAnnotation)
      textField.addKid(widgetAnnotation)
      textField.setFieldName(annotation.signer)
      form.addField(textField, pdfDoc.getPage(annotation.page))
    }

    // annotates will be added after close the document
    pdfDoc.close()

    // write to file
    //    import java.io.FileOutputStream
    //    val out = new FileOutputStream("/Users/eranga/Desktop/annotation.pdf")
    //    baos.writeTo(out)
    //    out.close()

    // annotated pdf as base64 encoded string
    new BASE64Encoder().encode(baos.toByteArray)
      .replaceAll("\n", "")
      .replaceAll("\r", "")
  }

}

//object M extends App {
//  val s = "eranga@gmail.com;10;20;1,asith@gmail.com;15;30;2"
//  val annotations = s.split(",").toList.map(_.trim)
//    .map(_.split(";").toList.map(_.trim))
//    .flatMap(p => AnnotateFactory.toAnnotate(p))
//
//  println(annotations)
//
//  println(annotations.map(_.signer).mkString(","))
//  println(annotations.map(_.signer.trim))
//}
