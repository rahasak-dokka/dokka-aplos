package com.score.aplos.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.score.aplos.actor
import com.score.aplos.actor.HttpActor.Serve
import com.score.aplos.cassandra.Credential
import com.score.aplos.config.AppConf
import com.score.aplos.protocol._
import com.score.aplos.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpActor {

  case class Serve()

  def props()(implicit system: ActorSystem) = Props(new HttpActor)

}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec = system.dispatcher

      Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: ActorMaterializer, ec: ExecutionContextExecutor) = {
    implicit val timeout = Timeout(30.seconds)

    import com.score.aplos.protocol.AccountMessageProtocol._
    import com.score.aplos.protocol.CredentialMessageProtocol._
    import com.score.aplos.protocol.StatusReplyProtocol._

    pathPrefix("api") {
      path("credentials") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "credential contract"))
        } ~
          post {
            entity(as[CredentialMessage]) {
              case create: CredentialActor.Create =>
                val f = context.actorOf(CredentialActor.props()) ? create
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"create credential status $status")
                    complete(if (status.code == 201) StatusCodes.Created -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail create credential")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case update: CredentialActor.Update =>
                val f = context.actorOf(CredentialActor.props()) ? update
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"update credential status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail update credential")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case updateBlob: actor.CredentialActor.UpdateBlob =>
                val f = context.actorOf(CredentialActor.props()) ? updateBlob
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"update credential blob status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail update credential blob")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case approve: CredentialActor.Approve =>
                val f = context.actorOf(CredentialActor.props()) ? approve
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"approve credential status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail approve credential")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case get: CredentialActor.Get =>
                val f = context.actorOf(CredentialActor.props()) ? get
                onComplete(f) {
                  case Success(credential: Credential) =>
                    logger.info(s"get reply $credential")
                    import com.score.aplos.protocol.CredentialProtocol._
                    complete(StatusCodes.OK -> credential)
                  case Success(status: StatusReply) =>
                    logger.info(s"get credential status $status")
                    complete(if (status.code == 404) StatusCodes.NotFound else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail to get credential ")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case getBlob: CredentialActor.GetBlob =>
                val f = context.actorOf(CredentialActor.props()) ? getBlob
                onComplete(f) {
                  case Success(blob: String) =>
                    logger.info(s"got blob reply")
                    complete(StatusCodes.OK -> blob)
                  case Success(status: StatusReply) =>
                    logger.info(s"get blob status $status")
                    complete(if (status.code == 404) StatusCodes.NotFound else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail to get blob ")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case search: CredentialActor.Search =>
                val f = context.actorOf(CredentialActor.props()) ? search
                onComplete(f) {
                  case Success(reply: CredentialSearchReply) =>
                    logger.info(s"credentials search reply $reply")
                    import com.score.aplos.protocol.CredentialSearchReplyProtocol._
                    complete(StatusCodes.OK -> reply)
                  case Success(status: StatusReply) =>
                    logger.info(s"credentials search status $status")
                    complete(if (status.code == 404) StatusCodes.NotFound else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail to search credentials")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
            }
          }
      } ~ path("accounts") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "account contract"))
        } ~
          post {
            entity(as[AccountMessage]) {
              case create: AccountActor.Create =>
                val f = context.actorOf(AccountActor.props()) ? create
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"create account status $status")
                    complete(if (status.code == 201) StatusCodes.Created -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail create account")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case register: AccountActor.Register =>
                val f = context.actorOf(AccountActor.props()) ? register
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"register account status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Success(token: TokenReply) =>
                    import com.score.aplos.protocol.TokenReplyProtocol._
                    logger.info(s"register account token $token")
                    complete(if (token.status == 200) StatusCodes.OK -> token else StatusCodes.BadRequest -> token)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail register account")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case activate: AccountActor.Activate =>
                val f = context.actorOf(AccountActor.props()) ? activate
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"activate account status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Success(token: TokenReply) =>
                    import com.score.aplos.protocol.TokenReplyProtocol._
                    logger.info(s"activate account token $token")
                    complete(if (token.status == 200) StatusCodes.OK -> token else StatusCodes.BadRequest -> token)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail activate account")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case connect: AccountActor.Connect =>
                val f = context.actorOf(AccountActor.props()) ? connect
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"activate account status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Success(token: TokenReply) =>
                    import com.score.aplos.protocol.TokenReplyProtocol._
                    logger.info(s"activate account token $token")
                    complete(if (token.status == 200) StatusCodes.OK -> token else StatusCodes.BadRequest -> token)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail activate account")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case update: AccountActor.Update =>
                val f = context.actorOf(AccountActor.props()) ? update
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"update account status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail update account")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case addRole: AccountActor.AddRole =>
                val f = context.actorOf(AccountActor.props()) ? addRole
                onComplete(f) {
                  case Success(status: StatusReply) =>
                    logger.info(s"addRole account status $status")
                    complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail activate account")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
              case search: AccountActor.Search =>
                val f = context.actorOf(AccountActor.props()) ? search
                onComplete(f) {
                  case Success(reply: AccountSearchReply) =>
                    logger.info(s"account search reply $reply")
                    import com.score.aplos.protocol.AccountSearchReplyProtocol._
                    complete(StatusCodes.OK -> reply)
                  case Success(status: StatusReply) =>
                    logger.info(s"account search status $status")
                    complete(if (status.code == 404) StatusCodes.NotFound else StatusCodes.BadRequest -> status)
                  case Failure(e) =>
                    logError(e)
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                  case _ =>
                    logger.info("fail to search accounts")
                    complete(StatusCodes.BadRequest -> StatusReply(400, "error"))
                }
            }
          }
      }
    }
  }
}
