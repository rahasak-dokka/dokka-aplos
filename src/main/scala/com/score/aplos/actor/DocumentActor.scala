package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import com.score.aplos.actor.DocumentActor._
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf}
import com.score.aplos.elastic._
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AnnotateFactory, AppLogger, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._

object DocumentActor {

  case class Create(messageType: String, execer: String, id: String, documentId: String, documentCreator: String, documentName: String, documentDescription: String,
                    documentCompany: String, documentDept: String, documentTyp: String, documentBlob: String, documentVerifier: String, documentSigners: String,
                    documentTags: String, documentParent: String) extends DocumentMessage

  case class Update(messageType: String, execer: String, id: String, documentId: String, documentName: String, documentDescription: String, documentCompany: String,
                    documentDept: String, documentTyp: String) extends DocumentMessage

  case class AddTag(messageType: String, execer: String, id: String, documentId: String, documentTag: String) extends DocumentMessage

  case class Verify(messageType: String, execer: String, id: String, documentId: String, documentVerifier: String, documentStatus: String) extends DocumentMessage

  case class Sign(messageType: String, execer: String, id: String, documentId: String, documentSigner: String, documentSignature: String, documentBlob: String,
                  documentComment: String, documentStatus: String) extends DocumentMessage

  case class Get(messageType: String, execer: String, id: String, documentId: String) extends DocumentMessage

  case class Search(messageType: String, execer: String, id: String, offset: Int, limit: Int, idTerm: String, dept: String, typ: String,
                    status: String, tag: String, sort: String = "descending") extends DocumentMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new DocumentActor)

}

class DocumentActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // first check double spend
      val id = s"${create.execer};DocumentActor;${create.id}"
      if (!CassandraStore.isDoubleSpend(create.execer, "DocumentActor", create.id) && RedisStore.set(id)) {
        CassandraStore.getDocument(create.documentId) match {
          case Some(p) =>
            logger.error(s"existing document with same id - $p")
            sender ! StatusReply(400, "Existing document id")
          case _ =>
            // create trans
            implicit val format: JsonFormat[Create] = jsonFormat15(Create)
            val trans = Trans(create.id, create.execer, "DocumentActor", "create", create.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // get annotations from signers
            val annotations = create.documentSigners.split(",").toList.map(_.trim)
              .map(_.split(";").toList.map(_.trim))
              .flatMap(p => AnnotateFactory.toAnnotate(p))
            val annotatedBlob = AnnotateFactory.annotate(annotations, create.documentBlob)

            // create blob and document on mystiko cassandra
            val blob = Blob(create.documentId, annotatedBlob)
            val document = Document(create.documentId, create.documentCreator, create.documentName, create.documentDescription,
              create.documentCompany, create.documentDept, create.documentTyp,
              create.documentTags.split(",").toList.map(_.trim),
              create.documentId, create.documentVerifier,
              annotations.map(_.signer.trim),
              List(), "pending", create.documentParent
            )
            CassandraStore.createBlob(blob)
            CassandraStore.createDocument(document)

            logger.info(s"create done $create")
            sender ! StatusReply(201, "Created")

            // notify to verifier
            doNotify(document.verifier, document.id)
        }
      } else {
        logger.error(s"double spend init - $create")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case update: Update =>
      logger.info(s"got update message - $update")

      // first check double spend
      val id = s"${update.execer};DocumentActor;${update.id}"
      if (!CassandraStore.isDoubleSpend(update.execer, "DocumentActor", update.id) && RedisStore.set(id)) {
        CassandraStore.getDocument(update.documentId) match {
          case Some(_) =>
            // create trans
            implicit val format: JsonFormat[Update] = jsonFormat9(Update)
            val trans = Trans(update.id, update.execer, "DocumentActor", "update", update.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // update document
            val document = Document(update.documentId, "", update.documentName, update.documentDescription,
              update.documentCompany, update.documentDept, update.documentTyp, List(), "", "", List(), List(), "pending", "")
            CassandraStore.updateDocument(document)

            logger.info(s"update done $update")
            sender ! StatusReply(200, "Updated")
          case _ =>
            logger.error(s"no matching document found to update - $update")
            sender ! StatusReply(400, "No document with id")
        }
      }

      context.stop(self)

    case addTag: AddTag =>
      logger.info(s"got addTag message - $addTag")

      // first check double spend
      val id = s"${addTag.execer};DocumentActor;${addTag.id}"
      if (!CassandraStore.isDoubleSpend(addTag.execer, "DocumentActor", addTag.id) && RedisStore.set(id)) {
        CassandraStore.getDocument(addTag.documentId) match {
          case Some(d) =>
            // create trans
            implicit val format: JsonFormat[AddTag] = jsonFormat5(AddTag)
            val trans = Trans(addTag.id, addTag.execer, "DocumentActor", "AddTag", addTag.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // add tag
            CassandraStore.addRole(d.id, addTag.documentTag)

            sender ! StatusReply(200, "Updated")
          case _ =>
            logger.error(s"no matching document found to addTag - $addTag")
            sender ! StatusReply(400, "No document with id")
        }
      } else {
        logger.error(s"double spend put $addTag")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case verify: Verify =>
      logger.info(s"got verify message - $verify")

      // first check double spend
      val id = s"${verify.execer};DocumentActor;${verify.id}"
      if (!CassandraStore.isDoubleSpend(verify.execer, "DocumentActor", verify.id) && RedisStore.set(id)) {
        CassandraStore.getDocument(verify.documentId) match {
          case Some(d) =>
            // check documentVerifier has legal officer role
            if (d.verifier == verify.documentVerifier) {
              // create trans
              implicit val format: JsonFormat[Verify] = jsonFormat6(Verify)
              val trans = Trans(verify.id, verify.execer, "DocumentActor", "Verify", verify.toJson.toString, "")
              CassandraStore.createTrans(trans)

              // verified/unverified
              CassandraStore.updateStatus(verify.documentId, verify.documentStatus)

              logger.info(s"document verified - $d")
              sender ! StatusReply(200, "Updated")

              // notify to next signer
              d.signers.find(p => !d.signatures.map(_.signer).contains(p)) match {
                case Some(s) =>
                  // notify to signer
                  doNotify(s, d.id)
                case None =>
                  // this means all signers signed
                  logger.info("all signers signed")
              }
            } else {
              logger.error(s"invalid document verifier - ${verify.documentVerifier}, actual verifier - ${d.verifier}")
              sender ! StatusReply(403, "Unauthorized verifier")
            }
          case _ =>
            logger.error(s"no matching document found to verify - $verify")
            sender ! StatusReply(400, "No document with id")
        }
      } else {
        logger.error(s"double spend put $verify")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case sign: Sign =>
      logger.info(s"got sign message - $sign")

      // first check double spend
      val id = s"${sign.execer};DocumentActor;${sign.id}"
      if (!CassandraStore.isDoubleSpend(sign.execer, "DocumentActor", sign.id) && RedisStore.set(id)) {
        CassandraStore.getDocument(sign.documentId) match {
          case Some(d) =>
            // create trans
            implicit val format: JsonFormat[Sign] = jsonFormat9(Sign)
            val trans = Trans(sign.id, sign.execer, "DocumentActor", "Sign", sign.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // sign
            val signature = Signature(sign.documentSigner, sign.documentStatus, sign.documentSignature, sign.documentComment)
            CassandraStore.updateSignature(d, signature)
            CassandraStore.updateBlob(sign.documentId, sign.documentBlob)

            // TODO approved/rejected based on all signers signs

            logger.info(s"document signed - $d")
            sender ! StatusReply(200, "Updated")

            // find notifier based on approved/rejected status
            var notifier: Option[String] = None
            sign.documentStatus match {
              case "rejected" =>
                // rejected - legal officer
                notifier = Option(d.verifier)
              case "approved" =>
                // approved - next signer
                // next signer should not be just signed user
                // next signer should not be already signed the document
                notifier = d.signers.find(p => !d.signatures.map(_.signer).contains(p) && p != signature.signer)
            }

            // notify
            notifier match {
              case Some(n) =>
                // notify to signer
                doNotify(n, d.id)
              case None =>
                // this means no notifier
                // all signers signed may be
                logger.info("no notifier to notify")
            }
          case _ =>
            logger.error(s"no matching document found to sign - $sign")
            sender ! StatusReply(400, "No document with id")
        }
      } else {
        logger.error(s"double spend sign $sign")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case get: Get =>
      logger.info(s"got get message - $get")

      CassandraStore.getDocument(get.documentId) match {
        case Some(d) =>
          CassandraStore.getBlob(d.blobId) match {
            case Some(blob) =>
              sender ! GetReply(d.id, d.creator, d.name, d.description, d.company, d.dept, d.typ,
                d.tags.mkString(","),
                blob.blob, d.signers.mkString(","), d.signatures, d.status, d.parent,
                DateFactory.formatToString(Option(d.timestamp), DateFactory.TIMESTAMP_FORMAT).getOrElse(""))
            case _ =>
              sender ! StatusReply(404, "Document blob Not found")
          }
        case _ =>
          logger.error(s"no matching document found - ${get.documentId}")
          sender ! StatusReply(404, "Document Not found")
      }

      context.stop(self)

    case search: Search =>
      logger.info(s"got search message - $search")

      // check execer account and roles
      CassandraStore.getAccount(search.execer) match {
        case Some(account) =>
          // wildcards
          var wildcards = List[Criteria]()
          if (!search.idTerm.isEmpty) wildcards = wildcards :+ Criteria("id", search.idTerm)

          // filters with pending state
          // add pending only for legal officer and admin
          var filters = List(Criteria("dept", search.dept), Criteria("typ", search.typ))
          if (search.execer.equalsIgnoreCase(admin) || account.roles.contains("legal_officer")) {
            filters = filters :+ Criteria("status", search.status)
          } else {
            Criteria("status", "pending", mustNot = true)
          }

          // add tag terms
          var terms = List[Criteria]()
          terms = terms :+ Criteria("tags", search.tag)

          // for legal officers and admins 'signer terms' and 'nested' should not be added
          // only add them for normal users
          var nested: Option[Nested] = None
          if (!account.roles.contains("legal_officer") && !search.execer.equalsIgnoreCase(admin)) {
            // only get documents matching with signer
            terms = terms :+ Criteria("signers", search.execer)

            // nested builds on status pending/not pending
            // for pending status we need to search signatures with mustNot match
            if (search.status.equalsIgnoreCase("pending")) {
              nested = Option(Nested("signatures", List(Criteria("signatures.signer", search.execer)), mustNot = true))
            } else {
              nested = Option(Nested("signatures", List(Criteria("signatures.signer", search.execer))))
            }
          }

          // sorts and pages
          val sorts = List(Sort("timestamp", if (search.sort.equalsIgnoreCase("ascending")) true else false))
          val page = Page(search.offset, search.limit)

          val docs = ElasticStore.getDocuments(terms, wildcards, filters, nested, List(), sorts, Option(page))
          val meta = Meta(search.offset, search.limit, docs.size, 3400)
          val searchReply = DocumentSearchReply(meta, docs.map(d => DocumentReply(d.id, d.creator, d.name, d.description, d.company, d.dept, d.typ,
            d.tags.mkString(","),
            d.blobId,
            d.signers.mkString(","), d.signatures, d.status, d.parent,
            DateFactory.formatToString(Option(d.timestamp), DateFactory.TIMESTAMP_FORMAT).getOrElse(""))))
          sender ! searchReply

        case None =>
          logger.error(s"not registered execer - ${search.execer}")
          sender ! StatusReply(403, "Unauthorized execer")
      }

      context.stop(self)
  }

  def doNotify(notifier: String, documentId: String): Int = {
    logger.info(s"send notification documentId - $documentId, notifier - $notifier, api - $notificationApi")

    CassandraStore.getDevice(notifier) match {
      case Some(d) =>
        serviceMode match {
          case "DEV" =>
            200
          case _ =>
            implicit val ex = context.dispatcher
            implicit val format = jsonFormat3(Notification)

            val notification = Notification(d.typ, d.tkn, documentId)

            val future = Http(context.system).singleRequest(
              HttpRequest(
                HttpMethods.POST,
                notificationApi,
                entity = HttpEntity(ContentTypes.`application/json`, notification.toJson.toString.getBytes()))
            ).map { response =>
              if (response.status == StatusCodes.OK) {
                logger.info(s"success send notification $notification")
                response.status.intValue
              } else {
                logger.info(s"fail send notification $notification, status ${response.status}")
                response.status.intValue
              }
            }.recover {
              case e =>
                logError(e)
                400
            }
            Await.result(future, 10.seconds)
        }
      case None =>
        logger.error(s"not device with $notifier to send notification")
        400
    }
  }

}
