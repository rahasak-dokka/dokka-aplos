package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import com.score.aplos.actor.AccountActor._
import com.score.aplos.cassandra.{Account, CassandraStore, Trans}
import com.score.aplos.config.AppConf
import com.score.aplos.elastic.{Criteria, ElasticStore, Page, Sort}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._

object AccountActor {

  case class Create(messageType: String, execer: String, uid: String, accountId: String, accountPassword: String, accountName: String, accountPhone: String, accountEmail: String, deviceToken: String, deviceType: String, accountRoles: String) extends AccountMessage

  case class Register(messageType: String, execer: String, uid: String, accountId: String, accountPassword: String, deviceToken: String, deviceType: String) extends AccountMessage

  case class Activate(messageType: String, execer: String, uid: String, accountId: String, accountSalt: String) extends AccountMessage

  case class Update(messageType: String, execer: String, uid: String, accountId: String, accountName: String, accountPhone: String, accountEmail: String) extends AccountMessage

  case class Connect(messageType: String, execer: String, uid: String, accountId: String, accountPassword: String) extends AccountMessage

  case class AddRole(messageType: String, execer: String, uid: String, accountId: String, accountRole: String) extends AccountMessage

  case class Get(messageType: String, execer: String, uid: String, accountId: String) extends AccountMessage

  case class Search(messageType: String, execer: String, uid: String, offset: Int, limit: Int, idTerm: String, phone: String, email: String, name: String) extends AccountMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new AccountActor)

}

class AccountActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got account create message - $create")

      // first check double spend
      val id = s"${create.execer};AccountActor;${create.uid}"
      if (!CassandraStore.isDoubleSpend(create.execer, "AccountActor", create.uid) && RedisStore.set(id)) {
        CassandraStore.getAccount(create.accountId) match {
          case Some(acc) =>
            // already existing account
            logger.error(s"account already exists - $acc")
            sender ! StatusReply(400, "Existing account with id")
          case None =>
            // create trans
            implicit val format: JsonFormat[Create] = jsonFormat11(Create)
            val salt = "93121"
            val trans = Trans(create.uid, create.execer, "AccountActor", "Create", create.toJson.toString, "digsig")
            CassandraStore.createTrans(trans)

            // create account
            val acc = Account(create.accountId, create.accountPassword, create.accountName, create.accountPhone, create.accountEmail,
              create.deviceType, create.deviceToken,
              create.accountRoles.split(",").toList.map(_.trim),
              salt, 0, activated = false, disabled = false)
            CassandraStore.createAccount(acc)

            sender ! StatusReply(201, "Created")
        }
      } else {
        logger.error(s"double spend create - $create")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case register: Register =>
      logger.info(s"got account register message - $register")

      // first check double spend
      val id = s"${register.execer};AccountActor;${register.uid}"
      if (!CassandraStore.isDoubleSpend(register.execer, "AccountActor", register.uid) && RedisStore.set(id)) {
        CassandraStore.getAccount(register.accountId) match {
          case Some(acc) =>
            // already existing account
            if (acc.activated) {
              // already registered account exists
              logger.error(s"activated account already exists - $register")
              sender ! StatusReply(400, "Activated account exists with id")
            } else {
              // found not activated account
              // create trans
              implicit val format: JsonFormat[Register] = jsonFormat7(Register)
              val trans = Trans(register.uid, register.execer, "AccountActor", "Register", register.toJson.toString, "digsig")
              CassandraStore.createTrans(trans)

              // update password and other fields
              val salt = "93121"
              val acc = Account(register.accountId, register.accountPassword, "", "", "", register.deviceType, register.deviceToken,
                List(), salt, 0, activated = true, disabled = false)
              CassandraStore.registerAccount(acc)

              // token reply
              implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
              val token = Token(acc.id, acc.roles.mkString(","), DateFactory.timestamp(), 60)
              sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
            }
          case None =>
            // should have created account to register from mobile
            logger.error(s"account does not exists to register - $register")
            sender ! StatusReply(400, "No account found with id")
        }
      } else {
        logger.error(s"double spend register - $register")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case activate: Activate =>
      logger.info(s"got account activate message - $activate")

      // first check double spend
      val id = s"${activate.execer};AccountActor;${activate.uid}"
      if (!CassandraStore.isDoubleSpend(activate.execer, "AccountActor", activate.uid) && RedisStore.set(id)) {
        CassandraStore.getAccount(activate.accountId) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.attempts > 3) {
              // no existing account
              logger.error(s"exceed account activate attempts - $acc")
              sender ! StatusReply(400, "Exceed attempts")
            } else {
              // compare salt
              if (acc.salt.equals(activate.accountSalt)) {
                // valid salt
                // create trans
                implicit val activateFormat: JsonFormat[Activate] = jsonFormat5(Activate)
                val trans = Trans(activate.uid, activate.execer, "AccountActor", "Activate", activate.toJson.toString, "")
                CassandraStore.createTrans(trans)

                // update attempts
                // activate account
                CassandraStore.updateAttempts(acc.id, acc.attempts + 1)
                CassandraStore.activateAccount(acc.id, activated = true)

                // token reply
                implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
                val token = Token(acc.id, acc.roles.mkString(","), DateFactory.timestamp(), 60)
                sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
              } else {
                // invalid salt
                logger.error(s"invalid salt to activate account - $acc")
                CassandraStore.updateAttempts(acc.id, acc.attempts + 1)

                sender ! StatusReply(401, "Invalid salt")
              }
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${activate.accountId}")

            sender ! StatusReply(401, "Not existing account")
        }
      } else {
        logger.error(s"double spend activate $activate")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case update: Update =>
      logger.info(s"got account update message - $update")

      // first check double spend
      val id = s"${update.execer};AccountActor;${update.uid}"
      if (!CassandraStore.isDoubleSpend(update.execer, "AccountActor", update.uid) && RedisStore.set(id)) {
        CassandraStore.getAccount(update.accountId) match {
          case Some(acc) =>
            // create trans
            implicit val activateFormat: JsonFormat[Update] = jsonFormat7(Update)
            val trans = Trans(update.uid, update.execer, "AccountActor", "Update", update.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // update
            CassandraStore.updateAccount(Account(acc.id, "", update.accountName, update.accountPhone, update.accountEmail, "", "", List(), "", 0, true, false))
            sender ! StatusReply(200, "Updated")
          case None =>
            // no existing account
            logger.error(s"no account found for - ${update.accountId}")

            sender ! StatusReply(401, "Not existing account")
        }
      } else {
        logger.error(s"double spend update $update")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case connect: Connect =>
      logger.info(s"got connect account message - $connect")

      // first check double spend
      val id = s"${connect.execer};AccountActor;${connect.uid}"
      if (!CassandraStore.isDoubleSpend(connect.execer, "AccountActor", connect.uid) && RedisStore.set(id)) {
        CassandraStore.getAccount(connect.accountId) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.activated) {
              if (acc.password.equals(connect.accountPassword)) {
                // token reply
                implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
                val token = Token(acc.id, acc.roles.mkString(","), DateFactory.timestamp(), 60)
                sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
              } else {
                logger.error(s"invalid password - $acc")
                CassandraStore.updateAttempts(acc.id, acc.attempts + 1)

                sender ! StatusReply(401, "Invalid credentials")
              }
            } else {
              // not activated acc
              logger.error(s"account not activated - $acc")
              sender ! StatusReply(400, "Account not activated")
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${connect.accountId}")

            sender ! StatusReply(401, "Not existing account")
        }
      } else {
        logger.error(s"double spend activate $connect")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case addRole: AddRole =>
      logger.info(s"got addRole account message - $addRole")

      // first check double spend
      val id = s"${addRole.execer};AccountActor;${addRole.uid}"
      if (!CassandraStore.isDoubleSpend(addRole.execer, "AccountActor", addRole.uid) && RedisStore.set(id)) {
        CassandraStore.getAccount(addRole.accountId) match {
          case Some(acc) =>
            // create trans
            implicit val format: JsonFormat[AddRole] = jsonFormat5(AddRole)
            val trans = Trans(addRole.uid, addRole.execer, "AccountActor", "AddRole", addRole.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // add role
            CassandraStore.addRole(acc.id, addRole.accountRole)

            sender ! StatusReply(200, "Updated")
          case None =>
            // no existing account
            logger.error(s"no account found for - ${addRole.accountId}")

            sender ! StatusReply(400, "Not existing account")
        }
      } else {
        logger.error(s"double spend addRole $addRole")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case search: Search =>
      logger.info(s"got document search message - $search")

      val criterias = List(Criteria("phone", search.phone), Criteria("email", search.email), Criteria("name", search.name))
      val page = Page(search.offset, search.limit)
      val sorts = List(Sort("timestamp", ascending = true))

      val terms = List()

      var wildcards = List[Criteria]()
      if (!search.idTerm.isEmpty) wildcards = wildcards :+ Criteria("id", search.idTerm)

      val (total, accs) = ElasticStore.getAccounts(terms, wildcards, criterias, None, List(), sorts, Option(page))
      val meta = AccountMeta(search.offset, search.limit, accs.size, total.toInt)
      val searchReply = AccountSearchReply(meta, accs.map(a =>
        AccountReply(a.id, a.name, a.phone, a.email, a.roles.mkString(","), a.activated, a.disabled, DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT).getOrElse(""))))
      sender ! searchReply

      context.stop(self)
  }

  def doSms(sms: Sms): Int = {
    serviceMode match {
      case "DEV" =>
        200
      case _ =>
        implicit val ex = context.dispatcher
        implicit val format = jsonFormat2(Sms)

        val future = Http(context.system).singleRequest(
          HttpRequest(
            HttpMethods.POST,
            "https://postman-echo.com/post",
            entity = HttpEntity(ContentTypes.`application/json`, sms.toJson.toString.getBytes()))
        ).map { response =>
          if (response.status == StatusCodes.OK) {
            logger.info(s"success transfer $sms")
            response.status.intValue
          } else {
            logger.info(s"fail send sms $sms, status ${response.status}")
            response.status.intValue
          }
        }.recover {
          case e =>
            logError(e)
            400
        }
        Await.result(future, 5.seconds)
    }
  }
}
