package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.score.aplos.actor.DeviceActor.{Create, Notify}
import com.score.aplos.cassandra.{CassandraStore, Device, Trans}
import com.score.aplos.config.{AppConf, FcmConf}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.AppLogger
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._

object DeviceActor {

  case class Create(messageType: String, execer: String, id: String, deviceId: String, deviceToken: String, deviceType: String, deviceOwner: String) extends DeviceMessage

  case class Notify(messageType: String, execer: String, id: String, notifyDevice: String, notifyMessage: String) extends DeviceMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new DeviceActor)

}

class DeviceActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with FcmConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got device create message - $create")

      // first check double spend
      val id = s"${create.execer};DeviceActor;${create.id}"
      if (!CassandraStore.isDoubleSpend(create.execer, "DeviceActor", create.id) && RedisStore.set(id)) {
        CassandraStore.getDevice(create.deviceId) match {
          case Some(device) =>
            // already existing device
            logger.error(s"device already exists - $device")
            sender ! StatusReply(400, "")
          case None =>
            // create trans
            implicit val format: JsonFormat[Create] = jsonFormat7(Create)
            val trans = Trans(create.id, create.execer, "DeviceActor", "Create", create.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // create device
            val device = Device(create.deviceId, create.deviceToken, create.deviceType, create.deviceOwner)
            CassandraStore.createDevice(device)

            sender ! StatusReply(201, "Created")
        }
      } else {
        logger.error(s"double spend create - $create")
        sender ! StatusReply(400, "")
      }

      context.stop(self)
    case notify: Notify =>
      logger.info(s"got device notify message - $notify")

      // first check double spend
      val id = s"${notify.execer};DeviceActor;${notify.id}"
      if (!CassandraStore.isDoubleSpend(notify.execer, "DeviceActor", notify.id) && RedisStore.set(id)) {
        CassandraStore.getDevice(notify.notifyDevice) match {
          case Some(device) =>
            // create trans
            implicit val format: JsonFormat[Notify] = jsonFormat5(Notify)
            val trans = Trans(notify.id, notify.execer, "DeviceActor", "Notify", notify.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // notify
            device.typ match {
              case "apple" =>
                val status = notifyDevice(FcmApple(device.tkn, contentAvailable = true, AppleNotification("New document", "New document received", notify.notifyMessage)))
                if (status == 200) {
                  sender ! StatusReply(200, "Notified apple")
                } else {
                  sender ! StatusReply(400, "Fail notify")
                }
              case "android" =>
                val status = notifyDevice(FcmAndroid(device.tkn, AndroidNotification("document_id")))
                if (status == 200) {
                  sender ! StatusReply(200, "Notified apple")
                } else {
                  sender ! StatusReply(400, "Fail notify")
                }
              case _ =>
                logger.error(s"invalid device type - $device")

                sender ! StatusReply(400, "Fail notify")
            }
          case None =>
            // no existing device
            logger.error(s"no device found for - ${notify.notifyDevice}")

            sender ! StatusReply(400, "")
        }
      } else {
        logger.error(s"double spend notify $notify")

        sender ! StatusReply(400, "")
      }

      context.stop(self)
  }

  def notifyDevice(fcm: Fcm): Int = {
    serviceMode match {
      case "DEV" =>
        200
      case _ =>
        implicit val ex = context.dispatcher
        import com.score.aplos.protocol.FcmProtocol._

        val future = Http(context.system).singleRequest(
          HttpRequest(
            HttpMethods.POST,
            fcmApi,
            entity = HttpEntity(ContentTypes.`application/json`, fcm.toJson.toString.getBytes()))
            .withHeaders(RawHeader("Content-Type", "application/json"), RawHeader("Authorization", s"key=$fcmKey"))
        ).map { response =>
          if (response.status == StatusCodes.OK) {
            logger.info(s"success fcm, $fcm")
            logger.info(s"success fcm response ${Unmarshal(response.entity).to[String]}")
            response.status.intValue
          } else {
            logger.info(s"fail fcm $fcm, status ${response.status}")
            logger.info(s"fail fcm response ${Unmarshal(response.entity).to[String]}")
            response.status.intValue
          }
        }.recover {
          case e =>
            logError(e)
            400
        }
        Await.result(future, 5.seconds)
    }
  }

  def notifyApple(fcmApple: FcmApple): Int = {
    serviceMode match {
      case "DEV" =>
        200
      case _ =>
        implicit val ex = context.dispatcher
        import com.score.aplos.protocol.FcmAppleProtocol._

        val future = Http(context.system).singleRequest(
          HttpRequest(
            HttpMethods.POST,
            fcmApi,
            entity = HttpEntity(ContentTypes.`application/json`, fcmApple.toJson.toString.getBytes()))
            .withHeaders(RawHeader("Content-Type", "application/json"), RawHeader("Authorization", s"key=$fcmKey"))
        ).map { response =>
          if (response.status == StatusCodes.OK) {
            logger.info(s"success fcm apple $fcmApple")
            response.status.intValue
          } else {
            logger.info(s"fail fcm apple $fcmApple, status ${response.status}")
            response.status.intValue
          }
        }.recover {
          case e =>
            logError(e)
            400
        }
        Await.result(future, 5.seconds)
    }
  }

  def notifyAndroid(fcmAndroid: FcmAndroid): Int = {
    serviceMode match {
      case "DEV" =>
        200
      case _ =>
        implicit val ex = context.dispatcher
        import com.score.aplos.protocol.FcmAndroidProtocol._

        val future = Http(context.system).singleRequest(
          HttpRequest(
            HttpMethods.POST,
            fcmApi,
            entity = HttpEntity(ContentTypes.`application/json`, fcmAndroid.toJson.toString.getBytes()))
            .withHeaders(RawHeader("Content-Type", "application/json"), RawHeader("Authorization", s"key=$fcmKey"))
        ).map { response =>
          if (response.status == StatusCodes.OK) {
            logger.info(s"success fcm android $fcmAndroid")
            response.status.intValue
          } else {
            logger.info(s"fail fcm android $fcmAndroid, status ${response.status}")
            response.status.intValue
          }
        }.recover {
          case e =>
            logError(e)
            400
        }
        Await.result(future, 5.seconds)
    }
  }
}

//object M extends App with FcmConf {
//
//  import akka.actor.ActorSystem
//  import com.score.aplos.util.LoggerFactory
//
//  // setup logging
//  LoggerFactory.init()
//
//  implicit val system = ActorSystem()
//  implicit val materializer = ActorMaterializer()
//  implicit val ec = system.dispatcher
//
//  def notify(fcm: Fcm): Int = {
//    import com.score.aplos.protocol.FcmProtocol._
//
//    val future = Http().singleRequest(
//      HttpRequest(
//        HttpMethods.POST,
//        "https://fcm.googleapis.com/fcm/send",
//        entity = HttpEntity(ContentTypes.`application/json`, fcm.toJson.toString.getBytes()))
//        .withHeaders(RawHeader("Content-Type", "application/json"), RawHeader("Authorization", s"key=$fcmKey"))
//    ).map { response =>
//      if (response.status == StatusCodes.OK) {
//        println(s"success fcm $fcm")
//        println(s"success fcm response ${Unmarshal(response.entity).to[String]}")
//        response.status.intValue
//      } else {
//        println(s"fail fcm $fcm, status ${response.status}")
//        println(s"fail fcm response ${Unmarshal(response.entity).to[String]}")
//        response.status.intValue
//      }
//    }.recover {
//      case e =>
//        e.printStackTrace()
//        400
//    }
//    Await.result(future, 5.seconds)
//  }
//
//  val tkn = "esXjFZ4YVZI:APA91bE-9TwKMV4RPpjGTQbOnPmiG0MshdpKeN6YBJesW3DLWKMhTAKspQPwHyw1AeXqEupypyi4F58ASTVWLwT5pPjjugVIOSsYdGJSEsfeFT2LFMP3ql4FwqDysoCUpqDvpc7SrKi0"
//  notify(FcmAndroid(tkn, AndroidNotification("1212")))
//
//}

