package com.score.aplos.actor

import java.util.UUID

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import com.score.aplos.actor.CredentialActor._
import com.score.aplos.cassandra.{Blob, CassandraStore, Credential, Trans}
import com.score.aplos.config.{ApiConf, AppConf}
import com.score.aplos.elastic.{Criteria, ElasticStore, Page, Sort}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.AppLogger
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._

object CredentialActor {

  case class Create(messageType: String, execer: String, uid: String,
                    id: String, owner: String,
                    name: String, address1: String, address2: String, city: String, state: String, zipCode: String,
                    phone: String, email: String, ssn: String, sex: String, birthDate: String, birthCity: String, birthState: String,
                    licenseType: String, licenseNo: String, licenseIssueDate: String, licenseExpireDate: String, daeNo: String, daeExpireDate: String,
                    boardCertificate: String, hireDate: String, serviceLocation: String, clinicalPractiseAreas: String, ageRange: String,
                    npiNo: String, npiUsername: String, npiPassword: String, caqhNo: String, caqhUsername: String, caqhPassword: String) extends CredentialMessage

  case class Update(messageType: String, execer: String, uid: String) extends CredentialMessage

  case class UpdateBlob(messageType: String, execer: String, uid: String, id: String, blobType: String, blobValue: String) extends CredentialMessage

  case class Approve(messageType: String, execer: String, uid: String, id: String, status: String, approver: String) extends CredentialMessage

  case class Get(messageType: String, execer: String, uid: String, id: String) extends CredentialMessage

  case class GetBlob(messageType: String, execer: String, uid: String, id: String) extends CredentialMessage

  case class Search(messageType: String, execer: String, uid: String,
                    offset: Int, limit: Int, idTerm: String, nameTerm: String, phone: String, email: String,
                    status: String, owner: String, sort: String = "descending") extends CredentialMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new CredentialActor)

}

class CredentialActor(implicit meterializer: ActorMaterializer) extends Actor with AppConf with ApiConf with AppLogger {

  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // first check double spend
      val id = s"${create.execer};CredentialActor;${create.uid}"
      if (!CassandraStore.isDoubleSpend(create.execer, "CredentialActor", create.uid) && RedisStore.set(id)) {
        CassandraStore.getCredential(create.id) match {
          case Some(p) =>
            logger.error(s"existing credentials with same id - $p")
            sender ! StatusReply(400, "Existing credential id")
          case _ =>
            // create trans
            import com.score.aplos.protocol.CreateProtocol._
            val trans = Trans(create.uid, create.execer, "CredentialActor", "create", create.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // create credential
            val credential = Credential(create.id, create.owner,
              create.name, create.address1, create.address2, create.city, create.state, create.zipCode,
              create.phone, create.email, create.ssn, create.sex, create.birthDate, create.birthCity, create.birthState,
              create.licenseType, create.licenseNo, create.licenseIssueDate, create.licenseExpireDate, create.daeNo, create.daeExpireDate,
              create.boardCertificate, create.hireDate, create.serviceLocation, create.clinicalPractiseAreas, create.ageRange,
              create.npiNo, create.npiUsername, create.npiPassword, create.caqhNo, create.caqhUsername, create.caqhPassword,
              "", "", "", "", "pending", "", null
            )
            CassandraStore.createCredential(credential)

            logger.info(s"create done $create")
            sender ! StatusReply(201, "Created")
        }
      } else {
        logger.error(s"double spend create $create")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case update: Update =>
      logger.info(s"got update message - $update")

      // first check double spend
      val id = s"${update.execer};CredentialActor;${update.uid}"
      if (!CassandraStore.isDoubleSpend(update.execer, "CredentialActor", update.uid) && RedisStore.set(id)) {
        // create trans
        implicit val format: JsonFormat[Update] = jsonFormat3(Update)
        val trans = Trans(update.uid, update.execer, "CredentialActor", "update", update.toJson.toString, "")
        CassandraStore.createTrans(trans)

        // update credentials

        logger.info(s"update done $update")
        sender ! StatusReply(200, "Updated")
      } else {
        logger.error(s"double spend update $update")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case approve: Approve =>
      logger.info(s"got approve message - $approve")

      // first check double spend
      val id = s"${approve.execer};CredentialActor;${approve.uid}"
      if (!CassandraStore.isDoubleSpend(approve.execer, "CredentialActor", approve.uid) && RedisStore.set(id)) {
        CassandraStore.getCredential(approve.id) match {
          case Some(_) =>
            // create trans
            implicit val format: JsonFormat[Approve] = jsonFormat6(Approve)
            val trans = Trans(approve.uid, approve.execer, "CredentialActor", "approve", approve.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // approve
            CassandraStore.approveCredentials(approve.status, approve.approver, approve.id)

            logger.info(s"approve done $approve")
            sender ! StatusReply(200, "Updated")
          case _ =>
            logger.error(s"no matching credential found to approve - $approve")
            sender ! StatusReply(400, "No credential with id")
        }
      } else {
        logger.error(s"double spend approve $approve")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case updateBlob: UpdateBlob =>
      logger.info(s"got updateBlob message - $updateBlob")

      // first check double spend
      val id = s"${updateBlob.execer};CredentialActor;${updateBlob.uid}"
      if (!CassandraStore.isDoubleSpend(updateBlob.execer, "CredentialActor", updateBlob.uid) && RedisStore.set(id)) {
        CassandraStore.getCredential(updateBlob.id) match {
          case Some(_) =>
            // create trans
            implicit val format: JsonFormat[UpdateBlob] = jsonFormat6(UpdateBlob)
            val trans = Trans(updateBlob.uid, updateBlob.execer, "CredentialActor", "updateBlob", updateBlob.toJson.toString, "")
            CassandraStore.createTrans(trans)

            // update blob
            val bId = UUID.randomUUID().toString
            val blob = Blob(bId, updateBlob.blobValue)
            CassandraStore.createBlob(blob)
            CassandraStore.updateCredentialBlob(updateBlob.blobType, bId, updateBlob.id)

            logger.info(s"update done $updateBlob")
            sender ! StatusReply(200, "Updated")
          case _ =>
            logger.error(s"no matching credential found to update - $updateBlob")
            sender ! StatusReply(400, "No credential with id")
        }
      } else {
        logger.error(s"double spend updateBlob $updateBlob")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case get: Get =>
      logger.info(s"got get message - $get")

      CassandraStore.getAccount(get.execer) match {
        case Some(account) =>
          CassandraStore.getCredential(get.id) match {
            case Some(c) =>
              if (account.roles.contains("admin") || c.owner == get.execer) {
                sender ! c
              } else {
                logger.error(s"unauthorized execer - ${get.execer}")
                sender ! StatusReply(403, "Unauthorized")
              }
            case _ =>
              logger.error(s"no matching Credential found - ${get.id}")
              sender ! StatusReply(404, "Credential Not found")
          }
        case None =>
          logger.error(s"not registered execer - ${get.execer}")
          sender ! StatusReply(403, "Unauthorized execer")
      }

      context.stop(self)

    case getBlob: GetBlob =>
      logger.info(s"got getBlob message - $getBlob")

      CassandraStore.getBlob(getBlob.id) match {
        case Some(b) =>
          sender ! b.blob
        case _ =>
          logger.error(s"no matching blob found - ${getBlob.id}")
          sender ! StatusReply(404, "Blob Not found")
      }

      context.stop(self)

    case search: Search =>
      logger.info(s"got search message - $search")

      // check execer account and roles
      CassandraStore.getAccount(search.execer) match {
        case Some(account) =>
          // wildcards
          var wildcards = List[Criteria]()
          if (!search.idTerm.isEmpty) wildcards = wildcards :+ Criteria("id", search.idTerm)
          if (!search.nameTerm.isEmpty) wildcards = wildcards :+ Criteria("name", search.nameTerm)

          // filters
          var filters = List(Criteria("phone", search.phone), Criteria("email", search.email))
          if (account.roles.contains("admin")) {
            // search with owner
            filters = filters :+ Criteria("owner", search.owner)
          } else {
            // only get credentials of execer unless admin
            filters = filters :+ Criteria("owner", search.execer)
          }

          // sorts and pages
          val sorts = List(Sort("create_time", if (search.sort.equalsIgnoreCase("ascending")) true else false))
          val page = Page(search.offset, search.limit)

          val (total, creds) = ElasticStore.getCredentials(List(), wildcards, filters, None, List(), sorts, Option(page))
          val meta = Meta(search.offset, search.limit, creds.size, total.toInt)
          val searchReply = CredentialSearchReply(meta, creds)
          sender ! searchReply
        case None =>
          logger.error(s"not registered execer - ${search.execer}")
          sender ! StatusReply(403, "Unauthorized execer")
      }

      context.stop(self)
  }

  def doNotify(notifier: String, credentialId: String): Int = {
    logger.info(s"send notification credential id - $credentialId, notifier - $notifier, api - $notificationApi")

    CassandraStore.getDevice(notifier) match {
      case Some(d) =>
        serviceMode match {
          case "DEV" =>
            200
          case _ =>
            implicit val ex = context.dispatcher
            implicit val format = jsonFormat3(Notification)

            val notification = Notification(d.typ, d.tkn, credentialId)

            val future = Http(context.system).singleRequest(
              HttpRequest(
                HttpMethods.POST,
                notificationApi,
                entity = HttpEntity(ContentTypes.`application/json`, notification.toJson.toString.getBytes()))
            ).map { response =>
              if (response.status == StatusCodes.OK) {
                logger.info(s"success send notification $notification")
                response.status.intValue
              } else {
                logger.info(s"fail send notification $notification, status ${response.status}")
                response.status.intValue
              }
            }.recover {
              case e =>
                logError(e)
                400
            }
            Await.result(future, 10.seconds)
        }
      case None =>
        logger.error(s"not device with $notifier to send notification")
        400
    }
  }

}
