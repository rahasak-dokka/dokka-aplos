package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Meta(offset: Int, limit: Int, count: Int, total: Int)

object MetaProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val metaFormat = jsonFormat4(Meta)
}

