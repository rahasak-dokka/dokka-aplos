package com.score.aplos.protocol

import com.score.aplos.cassandra.Credential
import spray.json.{DefaultJsonProtocol, JsObject, JsValue, RootJsonFormat, _}

case class CredentialSearchReply(meta: Meta, credentials: List[Credential])

object CredentialSearchReplyProtocol extends DefaultJsonProtocol {

  import CredentialProtocol._
  import MetaProtocol._

  implicit object CredentialSearchReplyFormat extends RootJsonFormat[CredentialSearchReply] {
    override def write(obj: CredentialSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("credentials", obj.credentials.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}

