package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Sms(to: String, message: String)

object SmsMessageProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val smsFormat = jsonFormat2(Sms)
}

