package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.score.aplos.cassandra.Signature
import spray.json.DefaultJsonProtocol

case class GetReply(id: String, creator: String, name: String, description: String, company: String, dept: String, typ: String, tags: String, blob: String,
                    signers: String, signatures: List[Signature], status: String, parent: String, timestamp: String)

object DocumentGetReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  import SignatureProtocol._

  implicit val format = jsonFormat14(GetReply)

}
