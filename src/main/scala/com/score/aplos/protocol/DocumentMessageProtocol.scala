package com.score.aplos.protocol

import com.score.aplos.actor.DocumentActor._
import spray.json._

trait DocumentMessage

object DocumentMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat15(Create)
  implicit val updateFormat: JsonFormat[Update] = jsonFormat9(Update)
  implicit val verifyFormat: JsonFormat[Verify] = jsonFormat6(Verify)
  implicit val signFormat: JsonFormat[Sign] = jsonFormat9(Sign)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)
  implicit val searchFormat: JsonFormat[Search] = jsonFormat11(Search)

  implicit object DocumentMessageFormat extends RootJsonFormat[DocumentMessage] {
    def write(obj: DocumentMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case u: Update => u.toJson
        case v: Verify => v.toJson
        case si: Sign => si.toJson
        case g: Get => g.toJson
        case s: Search => s.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): DocumentMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("update")) => json.convertTo[Update]
        case Seq(JsString("verify")) => json.convertTo[Verify]
        case Seq(JsString("sign")) => json.convertTo[Sign]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("search")) => json.convertTo[Search]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import WatchMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", "tisslot", "black", "eranga")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[WatchMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}

