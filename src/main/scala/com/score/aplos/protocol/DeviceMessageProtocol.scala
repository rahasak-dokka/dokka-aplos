package com.score.aplos.protocol

import com.score.aplos.actor.DeviceActor._
import spray.json._

trait DeviceMessage

object DeviceMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat7(Create)
  implicit val notifyFormat: JsonFormat[Notify] = jsonFormat5(Notify)

  implicit object DeviceMessageFormat extends RootJsonFormat[DeviceMessage] {
    def write(obj: DeviceMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case n: Notify => n.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): DeviceMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("notify")) => json.convertTo[Notify]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}
