package com.score.aplos.protocol

import com.score.aplos.cassandra.Credential
import com.score.aplos.util.DateFactory
import spray.json.{DefaultJsonProtocol, JsObject, JsString, JsValue, RootJsonFormat}

object CredentialProtocol extends DefaultJsonProtocol {

  implicit object CredentialFormat extends RootJsonFormat[Credential] {
    override def write(obj: Credential): JsValue = {
      JsObject(
        "id" -> JsString(obj.id),
        "owner" -> JsString(obj.owner),
        "name" -> JsString(obj.name),
        "address1" -> JsString(obj.address1),
        "address2" -> JsString(obj.address2),
        "city" -> JsString(obj.city),
        "state" -> JsString(obj.state),
        "zipCode" -> JsString(obj.zipCode),
        "phone" -> JsString(obj.phone),
        "email" -> JsString(obj.email),
        "ssn" -> JsString(obj.ssn),
        "sex" -> JsString(obj.sex),
        "birthDate" -> JsString(obj.birthDate),
        "birthCity" -> JsString(obj.birthCity),
        "birthState" -> JsString(obj.birthState),
        "licenseType" -> JsString(obj.licenseType),
        "licenseNo" -> JsString(obj.licenseNo),
        "licenseIssueDate" -> JsString(obj.licenseIssueDate),
        "licenseExpireDate" -> JsString(obj.licenseExpireDate),
        "daeNo" -> JsString(obj.daeNo),
        "daeExpireDate" -> JsString(obj.daeExpireDate),
        "boardCertificate" -> JsString(obj.boardCertificate),
        "hireDate" -> JsString(obj.hireDate),
        "serviceLocation" -> JsString(obj.serviceLocation),
        "clinicalPractiseAreas" -> JsString(obj.clinicalPractiseAreas),
        "ageRange" -> JsString(obj.ageRange),
        "npiNo" -> JsString(obj.npiNo),
        "npiUsername" -> JsString(obj.npiUsername),
        "npiPassword" -> JsString(obj.npiPassword),
        "caqhNo" -> JsString(obj.caqhNo),
        "caqhUsername" -> JsString(obj.caqhUsername),
        "caqhPassword" -> JsString(obj.caqhPassword),
        "license" -> JsString(if (obj.licenseBlob == null) "" else obj.licenseBlob),
        "previousInsurance" -> JsString(if (obj.previousInsuranceBlob == null) "" else obj.previousInsuranceBlob),
        "currentInsurance" -> JsString(if (obj.currentInsuranceBlob == null) "" else obj.currentInsuranceBlob),
        "resume" -> JsString(if (obj.resumeBlob == null) "" else obj.resumeBlob),
        "status" -> JsString(obj.status),
        "approver" -> JsString(if (obj.approver == null) "" else obj.approver),
        "approveTime" -> JsString(DateFactory.formatToString(Option(obj.approveTime), DateFactory.TIMESTAMP_FORMAT).getOrElse("")),
        "createTime" -> JsString(DateFactory.formatToString(Option(obj.createTime), DateFactory.TIMESTAMP_FORMAT).getOrElse(""))
      )
    }

    override def read(json: JsValue) = ???
  }

}

