package com.score.aplos.protocol

import com.score.aplos.actor.CredentialActor._
import spray.json._

trait CredentialMessage

object CredentialMessageProtocol extends DefaultJsonProtocol {

  implicit val updateFormat: JsonFormat[Update] = jsonFormat3(Update)
  implicit val updateBlobFormat: JsonFormat[UpdateBlob] = jsonFormat6(UpdateBlob)
  implicit val approveFormat: JsonFormat[Approve] = jsonFormat6(Approve)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)
  implicit val searchFormat: JsonFormat[Search] = jsonFormat12(Search)

  implicit object CredentialMessageFormat extends RootJsonFormat[CredentialMessage] {
    def write(obj: CredentialMessage): JsValue =
      JsObject((obj match {
        case c: Create =>
          import CreateProtocol._
          c.toJson
        case u: Update => u.toJson
        case ub: UpdateBlob => ub.toJson
        case g: Get => g.toJson
        case s: Search => s.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): CredentialMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) =>
          import CreateProtocol._
          json.convertTo[Create]
        case Seq(JsString("update")) => json.convertTo[Update]
        case Seq(JsString("updateBlob")) => json.convertTo[UpdateBlob]
        case Seq(JsString("approve")) => json.convertTo[Approve]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("search")) => json.convertTo[Search]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

object CreateProtocol extends DefaultJsonProtocol {

  implicit object CreateFormat extends RootJsonFormat[Create] {
    override def write(obj: Create): JsValue = {
      JsObject(
        "uid" -> JsString(obj.uid),
        "execer" -> JsString(obj.execer),
        "messageType" -> JsString(obj.messageType),
        "id" -> JsString(obj.id),
        "owner" -> JsString(obj.owner),
        "name" -> JsString(obj.name),
        "address1" -> JsString(obj.address1),
        "address2" -> JsString(obj.address2),
        "city" -> JsString(obj.city),
        "state" -> JsString(obj.state),
        "zipCode" -> JsString(obj.zipCode),
        "phone" -> JsString(obj.phone),
        "email" -> JsString(obj.email),
        "ssn" -> JsString(obj.ssn),
        "sex" -> JsString(obj.sex),
        "birthDate" -> JsString(obj.birthDate),
        "birthCity" -> JsString(obj.birthCity),
        "birthState" -> JsString(obj.birthState),
        "licenseType" -> JsString(obj.licenseType),
        "licenseNo" -> JsString(obj.licenseNo),
        "licenseIssueDate" -> JsString(obj.licenseIssueDate),
        "licenseExpireDate" -> JsString(obj.licenseExpireDate),
        "daeNo" -> JsString(obj.daeNo),
        "daeExpireDate" -> JsString(obj.daeExpireDate),
        "boardCertificate" -> JsString(obj.boardCertificate),
        "hireDate" -> JsString(obj.hireDate),
        "serviceLocation" -> JsString(obj.serviceLocation),
        "clinicalPractiseAreas" -> JsString(obj.clinicalPractiseAreas),
        "ageRange" -> JsString(obj.ageRange),
        "npiNo" -> JsString(obj.npiNo),
        "npiUsername" -> JsString(obj.npiUsername),
        "npiPassword" -> JsString(obj.npiPassword),
        "caqhNo" -> JsString(obj.caqhNo),
        "caqhUsername" -> JsString(obj.caqhUsername),
        "caqhPassword" -> JsString(obj.caqhPassword)
      )
    }

    def read(json: JsValue): Create = {
      val fields = json.asJsObject.fields
      Create(fields("messageType").convertTo[String], fields("execer").convertTo[String], fields("uid").convertTo[String],
        fields("id").convertTo[String], fields("owner").convertTo[String],
        fields("name").convertTo[String], fields("address1").convertTo[String], fields("address2").convertTo[String], fields("city").convertTo[String], fields("state").convertTo[String], fields("zipCode").convertTo[String],
        fields("phone").convertTo[String], fields("email").convertTo[String], fields("ssn").convertTo[String], fields("sex").convertTo[String], fields("birthDate").convertTo[String], fields("birthCity").convertTo[String], fields("birthState").convertTo[String],
        fields("licenseType").convertTo[String], fields("licenseNo").convertTo[String], fields("licenseIssueDate").convertTo[String], fields("licenseExpireDate").convertTo[String], fields("daeNo").convertTo[String], fields("daeExpireDate").convertTo[String],
        fields("boardCertificate").convertTo[String], fields("hireDate").convertTo[String], fields("serviceLocation").convertTo[String], fields("clinicalPractiseAreas").convertTo[String], fields("ageRange").convertTo[String],
        fields("npiNo").convertTo[String], fields("npiUsername").convertTo[String], fields("npiPassword").convertTo[String], fields("caqhNo").convertTo[String], fields("caqhUsername").convertTo[String], fields("caqhPassword").convertTo[String]
      )
    }
  }

}
