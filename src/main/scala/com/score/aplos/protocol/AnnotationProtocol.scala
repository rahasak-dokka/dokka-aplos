package com.score.aplos.protocol

case class Annotation(signer: String, x: Float, y: Float, page: Int)

