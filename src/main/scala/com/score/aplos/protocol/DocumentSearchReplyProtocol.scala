package com.score.aplos.protocol

import com.score.aplos.cassandra.Signature
import spray.json.{DefaultJsonProtocol, JsObject, JsValue, RootJsonFormat, _}

case class DocumentReply(id: String, creator: String, name: String, description: String, company: String, dept: String, typ: String, tags: String, blobId: String,
                         signers: String, signatures: List[Signature], status: String, parent: String, timestamp: String)

case class DocumentSearchReply(meta: Meta, documents: List[DocumentReply])

object DocumentSearchReplyProtocol extends DefaultJsonProtocol {

  import MetaProtocol._
  import SignatureProtocol._

  implicit val documentReplyFormat: JsonFormat[DocumentReply] = jsonFormat14(DocumentReply)

  implicit object DocumentSearchReplyFormat extends RootJsonFormat[DocumentSearchReply] {
    override def write(obj: DocumentSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("documents", obj.`documents`.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}

