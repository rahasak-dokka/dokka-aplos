package com.score.aplos.protocol

import com.score.aplos.actor.AccountActor._
import spray.json._

trait AccountMessage

object AccountMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat11(Create)
  implicit val registerFormat: JsonFormat[Register] = jsonFormat7(Register)
  implicit val activateFormat: JsonFormat[Activate] = jsonFormat5(Activate)
  implicit val updateFormat: JsonFormat[Update] = jsonFormat7(Update)
  implicit val connectFormat: JsonFormat[Connect] = jsonFormat5(Connect)
  implicit val addRoleFormat: JsonFormat[AddRole] = jsonFormat5(AddRole)
  implicit val searchFormat: JsonFormat[Search] = jsonFormat9(Search)

  implicit object AccountMessageFormat extends RootJsonFormat[AccountMessage] {
    def write(obj: AccountMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case r: Register => r.toJson
        case a: Activate => a.toJson
        case u: Update => u.toJson
        case c: Connect => c.toJson
        case r: AddRole => r.toJson
        case s: Search => s.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): AccountMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("register")) => json.convertTo[Register]
        case Seq(JsString("activate")) => json.convertTo[Activate]
        case Seq(JsString("update")) => json.convertTo[Update]
        case Seq(JsString("connect")) => json.convertTo[Connect]
        case Seq(JsString("addRole")) => json.convertTo[AddRole]
        case Seq(JsString("search")) => json.convertTo[Search]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import WatchMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", "tisslot", "black", "eranga")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[WatchMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}

