package com.score.aplos.protocol

import com.score.aplos.cassandra.Signature
import com.score.aplos.util.DateFactory
import spray.json.{DefaultJsonProtocol, JsObject, JsString, JsValue, RootJsonFormat}

object SignatureProtocol extends DefaultJsonProtocol {

  implicit object SignatureFormat extends RootJsonFormat[Signature] {
    def write(obj: Signature) = JsObject(
      "signer" -> JsString(obj.signer),
      "status" -> JsString(obj.status),
      "digsig" -> JsString(obj.digsig),
      "comment" -> JsString(obj.comment),
      "timestamp" -> JsString(DateFactory.formatToString(Option(obj.timestamp), DateFactory.TIMESTAMP_FORMAT).getOrElse(""))
    )

    def read(json: JsValue): Signature = ???
  }

}

