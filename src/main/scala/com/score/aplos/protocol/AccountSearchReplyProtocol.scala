package com.score.aplos.protocol

import spray.json.{DefaultJsonProtocol, JsObject, JsValue, RootJsonFormat, _}

case class AccountMeta(offset: Int, limit: Int, count: Int, total: Int)

case class AccountReply(id: String, name: String, phone: String, email: String, roles: String, activated: Boolean, disabled: Boolean, timestamp: String)

case class AccountSearchReply(meta: AccountMeta, accounts: List[AccountReply])

object AccountSearchReplyProtocol extends DefaultJsonProtocol {
  implicit val metaFormat: JsonFormat[AccountMeta] = jsonFormat4(AccountMeta)
  implicit val accountReplyFormat: JsonFormat[AccountReply] = jsonFormat8(AccountReply)

  implicit object AccountSearchReplyFormat extends RootJsonFormat[AccountSearchReply] {
    override def write(obj: AccountSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("accounts", obj.accounts.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}


