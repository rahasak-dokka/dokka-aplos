package com.score.aplos.cassandra

import java.util.{Collections, Date}

import com.datastax.driver.core.UDTValue
import com.datastax.driver.core.querybuilder.QueryBuilder
import com.score.aplos.config.{AppConf, SchemaConf}
import com.score.aplos.util.AppLogger

import scala.collection.JavaConverters._

case class Account(id: String, password: String, name: String, phone: String, email: String, deviceType: String, deviceToken: String, roles: List[String], salt: String, attempts: Int, activated: Boolean, disabled: Boolean, timestamp: Date = new Date)

case class Credential(id: String, owner: String,
                      name: String, address1: String, address2: String, city: String, state: String, zipCode: String,
                      phone: String, email: String, ssn: String, sex: String, birthDate: String, birthCity: String, birthState: String,
                      licenseType: String, licenseNo: String, licenseIssueDate: String, licenseExpireDate: String, daeNo: String, daeExpireDate: String,
                      boardCertificate: String, hireDate: String, serviceLocation: String, clinicalPractiseAreas: String, ageRange: String,
                      npiNo: String, npiUsername: String, npiPassword: String, caqhNo: String, caqhUsername: String, caqhPassword: String,
                      licenseBlob: String, previousInsuranceBlob: String, currentInsuranceBlob: String, resumeBlob: String,
                      status: String, approver: String, approveTime: Date, createTime: Date = new Date())

// signature
case class Signature(signer: String, status: String, digsig: String, comment: String, timestamp: Date = new Date())

// document status
//  1. pending (not display for signers)
//  2. verified (display for signers)
//  3. unverified (not display for signers)
//  4. approved (display for signers)
//  5. rejected (display for signers)
case class Document(id: String, creator: String, name: String, description: String, company: String, dept: String, typ: String,
                    tags: List[String], blobId: String, verifier: String, signers: List[String], signatures: List[Signature],
                    status: String, parent: String, timestamp: Date = new Date)

case class Blob(id: String, blob: String, timestamp: Date = new Date())

case class Device(id: String, tkn: String, typ: String, owner: String, timestamp: Date = new Date())

case class Trans(id: String, execer: String, actor: String, messageTyp: String, message: String, digsig: String, timestamp: Date = new Date)

object CassandraStore extends CassandraCluster with AppConf with SchemaConf with AppLogger {

  // transaction
  lazy val dsps = session.prepare("SELECT id FROM mystiko.trans WHERE execer = ? AND actor = ? AND id = ? LIMIT 1")
  lazy val ctps = session.prepare("INSERT INTO mystiko.trans(id, execer, actor, message_typ, message, digsig, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?)")

  // account
  lazy val caps = session.prepare("INSERT INTO mystiko.accounts(id, password, name, phone, email, device_type, device_token, roles, salt, attempts, activated, disabled, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
  lazy val raps = session.prepare("UPDATE mystiko.accounts SET password = ?, device_type = ?, device_token =?, activated=? WHERE id = ?")
  lazy val uaps = session.prepare("UPDATE mystiko.accounts SET name = ?, phone = ?, email =? WHERE id = ?")
  lazy val aaps = session.prepare("UPDATE mystiko.accounts SET activated = ? WHERE id = ?")
  lazy val daps = session.prepare("UPDATE mystiko.accounts SET disabled = ? WHERE id = ?")
  lazy val gaps = session.prepare("SELECT * FROM mystiko.accounts where id = ? LIMIT 1")
  lazy val uatps = session.prepare("UPDATE mystiko.accounts SET attempts = ? WHERE id = ?")
  lazy val urps = session.prepare("UPDATE mystiko.accounts SET roles = roles + ? WHERE id = ?")
  lazy val upps = session.prepare("UPDATE mystiko.accounts SET phone = ? WHERE id = ?")
  lazy val ueps = session.prepare("UPDATE mystiko.accounts SET email = ? WHERE id = ?")

  // devices
  lazy val cdeps = session.prepare("INSERT INTO mystiko.devices(id, tkn, typ, owner, timestamp) VALUES(?, ?, ?, ?, ?)")
  lazy val udeps = session.prepare("UPDATE mystiko.devices SET tkn = ? WHERE id = ?")
  lazy val gdeps = session.prepare("SELECT * FROM mystiko.devices where id = ? LIMIT 1")

  // credentials
  lazy val ccps = session.prepare("INSERT INTO mystiko.credentials(" +
    "id, owner, " +
    "name, address1, address2, city, state, zip_code, " +
    "phone, email, ssn, sex, birth_date, birth_city, birth_state, " +
    "license_type, license_no, license_issue_date, license_expire_date, dae_no, dae_expire_date, " +
    "board_certificate, hire_date, service_location, clinical_practise_areas, age_range, " +
    "npi_no, npi_username, npi_password, caqh_no, caqh_username, caqh_password, " +
    "status, create_time) " +
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
  lazy val ucps = session.prepare("UPDATE mystiko.credentials SET dae_no = ?, npi_no = ?, caqh_no = ? WHERE id = ?")
  lazy val uclbps = session.prepare("UPDATE mystiko.credentials SET license_blob = ? WHERE id = ?")
  lazy val ucpibps = session.prepare("UPDATE mystiko.credentials SET previous_insurance_blob = ? WHERE id = ?")
  lazy val uccibps = session.prepare("UPDATE mystiko.credentials SET current_insurance_blob = ? WHERE id = ?")
  lazy val ucrbps = session.prepare("UPDATE mystiko.credentials SET resume_blob = ? WHERE id = ?")
  lazy val acps = session.prepare("UPDATE mystiko.credentials SET status = ?, approver = ?, approve_time = ? WHERE id = ?")
  lazy val gcps = session.prepare("SELECT * FROM mystiko.credentials where id = ? LIMIT 1")

  // blob
  lazy val cbps = session.prepare("INSERT INTO mystiko.blobs(id, blob, timestamp) VALUES(?, ?, ?)")
  lazy val ubps = session.prepare("UPDATE mystiko.blobs SET blob = ? WHERE id = ?")
  lazy val gbps = session.prepare("SELECT * FROM mystiko.blobs where id = ? LIMIT 1")

  // document
  lazy val cdps = session.prepare("INSERT INTO mystiko.documents(id, creator, name, description, company, dept, typ, tags, blob_id, verifier, signers, status, parent, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
  lazy val udps = session.prepare("UPDATE mystiko.documents SET name = ?, description = ?, company = ?, dept = ?, typ = ? WHERE id = ?")
  lazy val usps = session.prepare("UPDATE mystiko.documents SET status = ? WHERE id = ?")
  lazy val utps = session.prepare("UPDATE mystiko.documents SET tags = tags + ? WHERE id = ?")
  lazy val gdps = session.prepare("SELECT * FROM mystiko.documents where id = ? LIMIT 1")

  def init() = {
    // create keyspace
    // create udt type
    // create table
    session.execute(schemaCreateKeyspace)
    session.execute(schemaCreateTypeTrans)
    session.execute(schemaCreateTableTrans)
    session.execute(schemaCreateTableAccounts)
    session.execute(schemaCreateTableDevices)
    session.execute(schemaCreateTableCredentials)
    session.execute(schemaCreateTableBlobs)

    // create admin users
    val acc = Account(admin, "admin", "admin", "+94775432015", admin, "", "",
      List("admin"), "", 0, activated = true, disabled = false)
    CassandraStore.createAccount(acc)
  }

  def isDoubleSpend(execer: String, actor: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(dsps.bind(execer, actor, id)).one()
    row != null
  }

  def createTrans(trans: Trans) = {
    session.execute(ctps.bind(trans.id, trans.execer, trans.actor, trans.messageTyp, trans.message,
      trans.digsig, trans.timestamp))
  }

  def createAccount(account: Account) = {
    session.execute(caps.bind(account.id, account.password, account.name, account.phone, account.email,
      account.deviceType, account.deviceToken,
      account.roles.toSet.asJava: java.util.Set[String],
      account.salt, account.attempts: java.lang.Integer,
      account.activated: java.lang.Boolean, account.disabled: java.lang.Boolean, account.timestamp))
  }

  def getAccount(name: String): Option[Account] = {
    val row = session.execute(gaps.bind(name)).one()
    if (row != null) Option(Account(row.getString("id"), row.getString("password"), row.getString("name"), row.getString("phone"),
      row.getString("email"),
      row.getString("device_type"),
      row.getString("device_token"),
      row.getSet("roles", classOf[String]).asScala.toList,
      row.getString("salt"), row.getInt("attempts"), row.getBool("activated"),
      row.getBool("disabled"), row.getTimestamp("timestamp")))
    else None
  }

  def registerAccount(account: Account) = {
    session.execute(raps.bind(account.password, account.deviceType, account.deviceToken, account.activated: java.lang.Boolean, account.id))
  }

  def updateAccount(account: Account) = {
    session.execute(uaps.bind(account.name, account.phone, account.email, account.id))
  }

  def updatePhone(id: String, phone: String) = {
    session.execute(upps.bind(phone, id))
  }

  def updateEmail(id: String, email: String) = {
    session.execute(ueps.bind(email, id))
  }

  def updateAttempts(id: String, attempts: Int) = {
    session.execute(uatps.bind(attempts: java.lang.Integer, id))
  }

  def activateAccount(id: String, activated: Boolean) = {
    session.execute(aaps.bind(activated: java.lang.Boolean, id))
  }

  def disableAccount(id: String, disabled: Boolean) = {
    session.execute(daps.bind(disabled: java.lang.Boolean, id))
  }

  def addRole(id: String, role: String) = {
    session.execute(urps.bind(Collections.singleton(role), id))
  }

  def createDevice(device: Device) = {
    session.execute(cdeps.bind(device.id, device.tkn, device.typ, device.owner, device.timestamp))
  }

  def updateDevice(id: String, tkn: String) = {
    session.execute(udeps.bind(tkn, id))
  }

  def getDevice(id: String): Option[Device] = {
    val row = session.execute(gdeps.bind(id)).one()
    if (row != null) Option(Device(row.getString("id"), row.getString("tkn"), row.getString("typ"),
      row.getString("owner"), row.getTimestamp("timestamp")))
    else None
  }

  def createCredential(credential: Credential) = {
    session.execute(ccps.bind(
      credential.id, credential.owner,
      credential.name, credential.address1, credential.address2, credential.city, credential.state, credential.zipCode,
      credential.phone, credential.email, credential.ssn, credential.sex, credential.birthDate, credential.birthCity, credential.birthState,
      credential.licenseType, credential.licenseNo, credential.licenseIssueDate, credential.licenseExpireDate, credential.daeNo, credential.daeExpireDate,
      credential.boardCertificate, credential.hireDate, credential.serviceLocation, credential.clinicalPractiseAreas, credential.ageRange,
      credential.npiNo, credential.npiUsername, credential.npiPassword, credential.caqhNo, credential.caqhUsername, credential.caqhPassword,
      credential.status, credential.createTime
    ))
  }

  def updateCredential(credential: Credential) = {
    session.execute(ucps.bind(
      credential.daeNo, credential.npiNo, credential.caqhNo, credential.id
    ))
  }

  def updateCredentialBlob(blobType: String, blob: String, id: String) = {
    blobType match {
      case "license" =>
        session.execute(uclbps.bind(blob, id))
      case "previousInsurance" =>
        session.execute(ucpibps.bind(blob, id))
      case "currentInsurance" =>
        session.execute(uccibps.bind(blob, id))
      case "resume" =>
        session.execute(ucrbps.bind(blob, id))
    }
  }

  def approveCredentials(status: String, approver: String, id: String) = {
    session.execute(acps.bind(status, approver, new Date(), id))
  }

  def getCredential(id: String): Option[Credential] = {
    val row = session.execute(gcps.bind(id)).one()
    if (row != null) {
      // credential
      Option(Credential(
        row.getString("id"), row.getString("owner"),
        row.getString("name"), row.getString("address1"), row.getString("address2"), row.getString("city"), row.getString("state"), row.getString("zip_code"),
        row.getString("phone"), row.getString("email"), row.getString("ssn"), row.getString("sex"), row.getString("birth_date"), row.getString("birth_city"), row.getString("birth_state"),
        row.getString("license_type"), row.getString("license_no"), row.getString("license_issue_date"), row.getString("license_expire_date"), row.getString("dae_no"), row.getString("dae_expire_date"),
        row.getString("board_certificate"), row.getString("hire_date"), row.getString("service_location"), row.getString("clinical_practise_areas"), row.getString("age_range"),
        row.getString("npi_no"), row.getString("npi_username"), row.getString("npi_password"), row.getString("caqh_no"), row.getString("caqh_username"), row.getString("caqh_password"),
        row.getString("license_blob"), row.getString("previous_insurance_blob"), row.getString("current_insurance_blob"), row.getString("resume_blob"),
        row.getString("status"), row.getString("approver"), row.getTimestamp("approve_time"), row.getTimestamp("create_time")))
    } else None
  }

  def createDocument(document: Document) = {
    session.execute(cdps.bind(document.id, document.creator, document.name, document.description,
      document.company, document.dept, document.typ,
      document.tags.toSet.asJava: java.util.Set[String],
      document.blobId,
      document.verifier,
      document.signers.toSet.asJava: java.util.Set[String],
      document.status,
      document.parent,
      document.timestamp))
  }

  def updateDocument(document: Document) = {
    session.execute(udps.bind(document.name, document.description, document.company, document.dept, document.typ, document.id))
  }

  def getDocument(id: String): Option[Document] = {
    val row = session.execute(gdps.bind(id)).one()
    if (row != null) {
      // get signatures
      val signatures = row.getSet("signatures", classOf[UDTValue]).asScala.map(s =>
        Signature(s.getString("signer"), s.getString("status"), s.getString("digsig"),
          s.getString("comment"), s.getTimestamp("timestamp"))
      ).toList

      // document
      Option(Document(row.getString("id"), row.getString("creator"), row.getString("name"),
        row.getString("description"), row.getString("company"), row.getString("dept"),
        row.getString("typ"),
        row.getSet("tags", classOf[String]).asScala.toList,
        row.getString("blob_id"),
        row.getString("verifier"),
        row.getSet("signers", classOf[String]).asScala.toList,
        signatures, row.getString("status"),
        row.getString("parent"),
        row.getTimestamp("timestamp")))
    } else None
  }

  def updateSignature(document: Document, signature: Signature): Unit = {
    // signature type
    val sigType = cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("signature")

    // signature
    val sig = sigType.newValue
      .setString("signer", signature.signer)
      .setString("status", signature.status)
      .setString("digsig", signature.digsig)
      .setString("comment", signature.comment)
      .setTimestamp("timestamp", signature.timestamp)

    // update query
    val statement = QueryBuilder.update(cassandraKeyspace, "documents")
      .`with`(QueryBuilder.add("signatures", sig))
      .where(QueryBuilder.eq("id", document.id))
    session.execute(statement)
  }

  def updateStatus(id: String, status: String) = {
    session.execute(usps.bind(status, id))
  }

  def addTag(id: String, tag: String) = {
    session.execute(utps.bind(Collections.singleton(tag), id))
  }

  def createBlob(blob: Blob) = {
    session.execute(cbps.bind(blob.id, blob.blob, blob.timestamp))
  }

  def updateBlob(id: String, blob: String) = {
    session.execute(ubps.bind(blob, id))
  }

  def getBlob(id: String): Option[Blob] = {
    val row = session.execute(gbps.bind(id)).one()
    if (row != null) Option(Blob(row.getString("id"), row.getString("blob"),
      row.getTimestamp("timestamp")))
    else None
  }

}

//object M extends App {
//  CassandraStore.init()
//  println(CassandraStore.isDoubleSpend("eranga", "AccountActor", "112"))
//
//  CassandraStore.createAccount(Account("eranga", "", "122111", "3122", "erangagmail", "", "", List("dev", "ops"), "salt", 0, false, false))
//  println(CassandraStore.getAccount("eranga"))
//
//  CassandraStore.registerAccount(Account("eranga", "password", "", "", "", "apple", "323e2323", List(), "", 0, true, false))
//  println(CassandraStore.getAccount("eranga"))
//
//  CassandraStore.updateAccount(Account("eranga", "", "lambda", "l08232333", "lam@gamil.com", "", "", List(), "", 0, true, false))
//  println(CassandraStore.getAccount("eranga"))
//
//  CassandraStore.updatePhone("eranga", "00000")
//  CassandraStore.updateEmail("eranga", "eeee@g")
//
//  CassandraStore.activateAccount("eranga", true)
//  CassandraStore.updateAttempts("eranga", 2)
//  CassandraStore.disableAccount("eranga", true)
//  CassandraStore.createTrans(Trans("112", "eranga", "AccountActor", "create", "{\"message\": \"java\"}", "digsig"))
//
//  CassandraStore.createDevice(Device("121232", "tkn332", "android", "eranga"))
//  println(CassandraStore.getDevice("121232"))
//
//  val c = Credential("2222", "erangaeb@gmail.com",
//    "eranga bandara", "wewe", "", "suff", "ca", "3223",
//    "7743223", "eragna@gmail.com", "ee232", "male", "29/12/1987", "suff", "ca",
//    "ela", "232", "2342", "23423", "333", "234",
//    "332", "2019-10-17", "2324", "2342", "2342",
//    "23423", "2342", "2342", "23423", "2342", "111",
//    "", "", "", "", "pending", "", new Date(), new Date()
//  )
//  CassandraStore.createCredential(c)
//  println(CassandraStore.getCredential("1111"))
//}
