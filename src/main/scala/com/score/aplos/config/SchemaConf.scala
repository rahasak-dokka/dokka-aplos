package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait SchemaConf {
  val schemaConf = ConfigFactory.load("schema.conf")

  // schemas
  lazy val schemaCreateKeyspace = Try(schemaConf.getString("schema.createKeyspace")).getOrElse("")
  lazy val schemaCreateTypeTrans = Try(schemaConf.getString("schema.createTypeTrans")).getOrElse("")
  lazy val schemaCreateTableTrans = Try(schemaConf.getString("schema.createTableTrans")).getOrElse("")
  lazy val schemaCreateTableAccounts = Try(schemaConf.getString("schema.createTableAccounts")).getOrElse("")
  lazy val schemaCreateTableDevices = Try(schemaConf.getString("schema.createTableDevices")).getOrElse("")
  lazy val schemaCreateTableCredentials = Try(schemaConf.getString("schema.createTableCredentials")).getOrElse("")
  lazy val schemaCreateTableBlobs = Try(schemaConf.getString("schema.createTableBlobs")).getOrElse("")
}