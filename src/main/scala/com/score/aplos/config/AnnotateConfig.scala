package com.score.aplos.config

trait AnnotateConfig {
  val width = 612.0f
  val height = 792.0f
}
