package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait ApiConf {

  // config object
  val fcmConf = ConfigFactory.load("api.conf")

  // fcm config
  lazy val notificationApi = Try(fcmConf.getString("api.notification")).getOrElse("http://dev.localhost:8762/api/notifications")

}
