package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait FcmConf {

  // config object
  val fcmConf = ConfigFactory.load("fcm.conf")

  // fcm config
  lazy val fcmApi = Try(fcmConf.getString("fcm.api")).getOrElse("https://fcm.googleapis.com/fcm/send")
  lazy val fcmKey = Try(fcmConf.getString("fcm.key")).getOrElse("")

}
