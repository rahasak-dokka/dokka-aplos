package com.score.aplos.config

import com.typesafe.config.ConfigFactory

trait ElasticConf {
  val elasticConf = ConfigFactory.load("elastic.conf")

  lazy val elasticCluster = elasticConf.getString("elastic.cluster")

  lazy val transElasticIndex = elasticConf.getString("elastic.trans-index")
  lazy val transElasticDocType = elasticConf.getString("elastic.trans-doc-type")
  lazy val credentialsElasticIndex = elasticConf.getString("elastic.credentials-index")
  lazy val credentialsElasticDocType = elasticConf.getString("elastic.credentials-doc-type")
  lazy val accountsElasticIndex = elasticConf.getString("elastic.accounts-index")
  lazy val accountsElasticDocType = elasticConf.getString("elastic.accounts-doc-type")
  lazy val devicesElasticIndex = elasticConf.getString("elastic.devices-index")
  lazy val devicesElasticDocType = elasticConf.getString("elastic.devices-doc-type")

  lazy val elasticHosts = elasticConf.getString("elastic.hosts").split(",").toSeq.map(_.trim)
  lazy val elasticPort = elasticConf.getString("elastic.port").toInt
}