package com.score.aplos.elastic

import java.util

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.{GET, PUT}
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.score.aplos.cassandra.{Account, Credential, Document, Signature}
import com.score.aplos.config.{CassandraConf, ElasticConf}
import com.score.aplos.util.{AppLogger, DateFactory}
import org.apache.lucene.search.join.ScoreMode
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.sort.SortOrder

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration._

case class Sort(field: String, ascending: Boolean)

case class Criteria(name: String, value: String, mustNot: Boolean = false)

case class Nested(path: String, criterias: List[Criteria], mustNot: Boolean = false)

case class Range(name: String, from: String, to: String, format: String)

case class Page(offset: Int, limit: Int)

object ElasticStore extends ElasticCluster with AppLogger with CassandraConf with ElasticConf {

  def init()(implicit system: ActorSystem): Unit = {
    initIndex(transElasticIndex, transElasticDocType)
    initIndex(credentialsElasticIndex, credentialsElasticDocType)
    initIndex(accountsElasticIndex, accountsElasticDocType)
    initIndex(devicesElasticIndex, devicesElasticDocType)
  }

  def initIndex(index: String, docTyp: String)(implicit system: ActorSystem): Unit = {
    implicit val ec = system.dispatcher
    implicit val materializer = ActorMaterializer()
    implicit val timeout = 40.seconds

    // params
    val uri = s"http://${elasticHosts.head}:9200/$index"
    val json =
      s"""
      {
        "settings":{
          "keyspace": "$cassandraKeyspace"
        },
        "mappings": {
          "$docTyp" : {
            "discover" : ".*"
          }
        }
      }
    """

    // check index exists
    val get = HttpRequest(GET, uri = uri)
    Await.result(Http().singleRequest(get), timeout).status match {
      case StatusCodes.NotFound =>
        // index not exists
        logger.info(s"init index request uri $uri json $json")

        // create index
        val put = HttpRequest(PUT, uri = uri).withEntity(ContentTypes.`application/json`, ByteString(json.stripLineEnd))
        val resp = Await.result(Http().singleRequest(put), timeout)

        logger.info(s"init index response: $resp")
      case _ =>
        // index already exists
        logger.info(s"$index index already exists")
    }
  }

  def dehydrate(value: String): String = {
    if (value == null) "" else value
  }

  def getDocuments(terms: List[Criteria], wildcards: List[Criteria], filters: List[Criteria], nested: Option[Nested], ranges: List[Range], sorts: List[Sort], page: Option[Page]): List[Document] = {
    def toList(obj: Object): List[String] = {
      obj match {
        case strings: util.ArrayList[String] =>
          // multiple elements
          strings.asScala.toList
        case string: String =>
          // single elements
          List(string)
        case _ =>
          List()
      }
    }

    def toUdtList(obj: Object): List[Signature] = {
      obj match {
        case objl: util.ArrayList[util.Map[String, AnyRef]] =>
          // multiple elements
          objl.asScala.map { o =>
            Signature(o.get("signer").asInstanceOf[String],
              o.get("status").asInstanceOf[String],
              o.get("digsig").asInstanceOf[String],
              o.get("comment").asInstanceOf[String],
              DateFactory.formatToDate(o.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E)
            )
          }.toList
        case obj: util.Map[String, AnyRef] =>
          // single elements
          List(Signature(obj.get("signer").asInstanceOf[String],
            obj.get("status").asInstanceOf[String],
            obj.get("digsig").asInstanceOf[String],
            obj.get("comment").asInstanceOf[String],
            DateFactory.formatToDate(obj.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E)
          ))
        case _ =>
          List()
      }
    }

    // extract documents from search response
    val search = buildSearch(credentialsElasticIndex, credentialsElasticDocType, terms, wildcards, filters, nested, ranges, sorts, page)
    val resp = search.execute().actionGet()
    val hits = resp.getHits.getHits
    hits.filter(h => h.getSourceAsMap != null)
      .map { hit =>
        val h = hit.getSourceAsMap
        Document(
          h.get("id").asInstanceOf[String],
          h.get("creator").asInstanceOf[String],
          h.get("name").asInstanceOf[String],
          dehydrate(h.get("description").asInstanceOf[String]),
          dehydrate(h.get("company").asInstanceOf[String]),
          dehydrate(h.get("dept").asInstanceOf[String]),
          dehydrate(h.get("typ").asInstanceOf[String]),
          toList(h.get("tags")),
          h.get("blob_id").asInstanceOf[String],
          dehydrate(h.get("verifier").asInstanceOf[String]),
          toList(h.get("signers")),
          toUdtList(h.get("signatures")),
          h.get("status").asInstanceOf[String],
          dehydrate(h.get("parent").asInstanceOf[String]),
          DateFactory.formatToDate(h.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E)
        )
      }.toList
  }


  def getCredentials(terms: List[Criteria], wildcards: List[Criteria], filters: List[Criteria], nested: Option[Nested], ranges: List[Range], sorts: List[Sort], page: Option[Page]): (Long, List[Credential]) = {
    // extract documents from search response
    val search = buildSearch(credentialsElasticIndex, credentialsElasticDocType, terms, wildcards, filters, nested, ranges, sorts, page)
    val resp = search.execute().actionGet()
    val hits = resp.getHits.getHits
    val credentials = hits.filter(h => h.getSourceAsMap != null)
      .map { hit =>
        val h = hit.getSourceAsMap
        Credential(
          h.get("id").asInstanceOf[String], h.get("owner").asInstanceOf[String],
          dehydrate(h.get("name").asInstanceOf[String]), dehydrate(h.get("address1").asInstanceOf[String]), dehydrate(h.get("address2").asInstanceOf[String]), dehydrate(h.get("city").asInstanceOf[String]), dehydrate(h.get("state").asInstanceOf[String]), dehydrate(h.get("zip_code").asInstanceOf[String]),
          dehydrate(h.get("phone").asInstanceOf[String]), dehydrate(h.get("email").asInstanceOf[String]), dehydrate(h.get("ssn").asInstanceOf[String]), dehydrate(h.get("sex").asInstanceOf[String]), dehydrate(h.get("birth_date").asInstanceOf[String]), dehydrate(h.get("birth_city").asInstanceOf[String]), dehydrate(h.get("birth_state").asInstanceOf[String]),
          dehydrate(h.get("license_type").asInstanceOf[String]), dehydrate(h.get("license_no").asInstanceOf[String]), dehydrate(h.get("license_issue_date").asInstanceOf[String]), dehydrate(h.get("license_expire_date").asInstanceOf[String]), dehydrate(h.get("dae_no").asInstanceOf[String]), dehydrate(h.get("dae_expire_date").asInstanceOf[String]),
          dehydrate(h.get("board_certificate").asInstanceOf[String]), dehydrate(h.get("hire_date").asInstanceOf[String]), dehydrate(h.get("service_location").asInstanceOf[String]), dehydrate(h.get("clinical_practise_areas").asInstanceOf[String]), dehydrate(h.get("age_range").asInstanceOf[String]),
          dehydrate(h.get("npi_no").asInstanceOf[String]), dehydrate(h.get("npi_username").asInstanceOf[String]), dehydrate(h.get("npi_password").asInstanceOf[String]), dehydrate(h.get("caqh_no").asInstanceOf[String]), dehydrate(h.get("caqh_username").asInstanceOf[String]), dehydrate(h.get("caqh_password").asInstanceOf[String]),
          dehydrate(h.get("license_blob").asInstanceOf[String]), dehydrate(h.get("previous_insurance_blob").asInstanceOf[String]), dehydrate(h.get("current_insurance_blob").asInstanceOf[String]), dehydrate(h.get("resume_blob").asInstanceOf[String]),
          dehydrate(h.get("status").asInstanceOf[String]), dehydrate(h.get("approver").asInstanceOf[String]),
          if (h.get("approve_time") == null) null else DateFactory.formatToDate(h.get("approve_time").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E),
          DateFactory.formatToDate(h.get("create_time").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E)
        )
      }.toList
    (resp.getHits.totalHits, credentials)
  }

  def getAccounts(terms: List[Criteria], wildcards: List[Criteria], filters: List[Criteria], nested: Option[Nested], ranges: List[Range], sorts: List[Sort], page: Option[Page]): (Long, List[Account]) = {
    def toList(obj: Object): List[String] = {
      obj match {
        case strings: util.ArrayList[String] =>
          // multiple elements
          strings.asScala.toList
        case string: String =>
          // single elements
          List(string)
        case _ =>
          List()
      }
    }

    // extract documents from search response
    val search = buildSearch(accountsElasticIndex, accountsElasticDocType, terms, wildcards, filters, nested, ranges, sorts, page)
    val resp = search.execute().actionGet()

    val hits = resp.getHits.getHits
    val accounts = hits.filter(h => h.getSourceAsMap != null)
      .map { hit =>
        val h = hit.getSourceAsMap
        Account(
          h.get("id").asInstanceOf[String],
          "",
          h.get("name").asInstanceOf[String],
          dehydrate(h.get("phone").asInstanceOf[String]),
          dehydrate(h.get("email").asInstanceOf[String]),
          dehydrate(h.get("device_type").asInstanceOf[String]),
          dehydrate(h.get("device_token").asInstanceOf[String]),
          toList(h.get("roles")),
          "",
          h.get("attempts").asInstanceOf[Int],
          h.get("activated").asInstanceOf[Boolean],
          h.get("disabled").asInstanceOf[Boolean],
          DateFactory.formatToDate(h.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E)
        )
      }.toList
    (resp.getHits.totalHits, accounts)
  }

  def buildSearch(index: String, docType: String, terms: List[Criteria], wildcards: List[Criteria], filters: List[Criteria], nested: Option[Nested], ranges: List[Range], sorts: List[Sort], page: Option[Page]) = {
    // signers/tags term query
    // terms use with nested arrays
    val termBuilder = QueryBuilders.boolQuery()
    terms.filter(_.value.nonEmpty).foreach { f =>
      termBuilder.must(QueryBuilders.termQuery(f.name, f.value))
    }

    // wildcard
    val wildcardBuilder = QueryBuilders.boolQuery()
    wildcards.filter(_.value.nonEmpty).foreach { f =>
      wildcardBuilder.should(QueryBuilders.wildcardQuery(f.name, f.value))
    }

    // filter
    val filterBuilder = QueryBuilders.boolQuery()
    filters.filter(_.value.nonEmpty).foreach { f =>
      if (f.mustNot)
        filterBuilder.mustNot(QueryBuilders.matchQuery(f.name, f.value))
      else
        filterBuilder.must(QueryBuilders.matchQuery(f.name, f.value))
    }

    // range
    val rangeBuilder = QueryBuilders.boolQuery()
    ranges.foreach { r =>
      rangeBuilder.must(QueryBuilders.rangeQuery(r.name).from(r.from).to(r.to).format(r.format))
    }

    // combine builders with nested
    val builder = QueryBuilders.boolQuery()
    nested match {
      case Some(n) =>
        val nestedFilters = QueryBuilders.boolQuery()
        n.criterias.foreach { f =>
          nestedFilters.must(QueryBuilders.matchQuery(f.name, f.value))
        }

        // nested builder
        val nestedBuilder = QueryBuilders.boolQuery()
        if (n.mustNot)
          nestedBuilder.mustNot(QueryBuilders.nestedQuery(n.path, nestedFilters, ScoreMode.None))
        else
          nestedBuilder.must(QueryBuilders.nestedQuery(n.path, nestedFilters, ScoreMode.None))

        // combine all builders
        builder.must(termBuilder).must(nestedBuilder).must(filterBuilder).must(wildcardBuilder).must(rangeBuilder)
      case None =>
        // combine all builders
        builder.must(termBuilder).must(filterBuilder).must(wildcardBuilder).must(rangeBuilder)
    }

    // build search
    val searchBuilder = client
      .prepareSearch(index)
      .setTypes(docType)
      .setQuery(builder)

    // add sorts
    sorts.foreach { s =>
      searchBuilder.addSort(s.field, if (s.ascending) SortOrder.ASC else SortOrder.DESC)
    }

    // build pagination
    val p = page.getOrElse(Page(0, 10))
    searchBuilder.setFrom(p.offset).setSize(p.limit)
  }

}

//object M extends App {
//  //println(ElasticStore.getAccounts(List(), List(Criteria("id", "eranga*")), List(Criteria("name", "")), None, List(Sort("timestamp", ascending = false)), Option(Page(0, 4))))
//  println(ElasticStore.getCredentials(
//    List(),
//    List(),
//    List(),
//    None,
//    List(Range("hire_date", "2019-09-10", "2019-10-30", "yyyy-MM-dd")),
//    List(Sort("create_time", ascending = false)),
//    Option(Page(0, 4))))
//}
