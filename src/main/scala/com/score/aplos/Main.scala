package com.score.aplos

import akka.actor.ActorSystem
import com.score.aplos.actor.HttpActor
import com.score.aplos.actor.HttpActor.Serve
import com.score.aplos.cassandra.CassandraStore
import com.score.aplos.config.AppConf
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.util.{LoggerFactory, CryptoFactory}

object Main extends App with AppConf {

  // setup logging
  LoggerFactory.init()

  // actor system mystiko
  implicit val system = ActorSystem.create("mystiko")

  // set up keys
  CryptoFactory.init()

  // create schema/indexes
  CassandraStore.init()
  ElasticStore.init()

  system.actorOf(HttpActor.props(), name = "HttpActor") ! Serve

}

